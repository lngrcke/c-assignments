#include <stdio.h>     // Loana Gericke, s1643661, 31.10.16, Forrest Hill
#include <stdlib.h>

void Rotate(int b[], int n) {
  /* Aim:  rotate the elements of a array cyclically. */
  int i;
  int temp;      /* Temporary storage (like in swap). */

  printf ("The start address of the b array from the Rotate function is %p.\n", b);

  temp = b[n - 1];
  for (i = n - 1;  i > 0;  --i) { b[i] = b[i - 1]; }
  b[0] = temp;
}

int main(void) {

  int primes[10] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
  
  printf ("The start address of the primes array from main is %p.\n", primes);
  Rotate(primes, 10);

  printf ("The sequence of entries in primes is: ");
  int n;
  for ( n = 0 ; n < 9 ; n++) {
    printf ("%d, ", primes[n]);
  }
  printf ("%d.\n", primes[n++]);

  Rotate(primes, 5);
  printf ("The sequence of entries in primes is: ");
  for ( n = 0 ; n < 9 ; n++ ) {
    printf ("%d, ", primes[n]);
  } 
  printf ("%d.\n", primes[n++]);

 return EXIT_SUCCESS;
}
