#include <stdio.h>     // Loana Gericke, s1643661, 26.10.16, Forresthill
#include <stdlib.h>
#include "descartes.h"

int main(void) {
  point_t p, q, r, s;
  lineSeg_t pq, qr, rs, sp;

  OpenGraphics();
  printf ("Please click at two points with the left mouse button. \n");
  p = GetPoint();
  r = GetPoint();
  
  q = Point(XCoord(r), YCoord(p));
  s = Point(XCoord(p), YCoord(r));   // computing vertices q and s

  pq = LineSeg(p, q);
  qr = LineSeg(q, r);
  rs = LineSeg(r, s);
  sp = LineSeg(s, p);                // form edges 

  DrawLineSeg(pq);
  DrawLineSeg(qr);
  DrawLineSeg(rs);
  DrawLineSeg(sp);                   // draw rectangle

  double Length(lineSeg_t pq);
  printf ("The diagonal of the rectangle has %lf. \n", Length(pq));
                                     // determine the length of the diagonal
  double Length(lineSeg_t sp);
  if (Length(sp) >= 1.25 * Length(pq)) {
    printf ("The rectangle is tall.\n");
  } else if (Length(pq) >= 1.25 * Length(sp)) {
    printf ("The rectangle is wide. \n");
  } else {
    printf ("The rectangle is almost square. \n");
  }
 
  CloseGraphics();
 return EXIT_SUCCESS;
}
