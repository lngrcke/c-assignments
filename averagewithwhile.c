#include <stdio.h>           // Loana Gericke, s1643661, 24.10.16, Forresthill
#include <stdlib.h>

int main(void) {
  printf ("Welcome. You will be asked to input integers one at a time.\nTo finish type a letter or any non-digit.\n") ;
 
  int x ;                  // variable for input
  double count ; count = 1 ;  // variable for count of inputs
  double sum ; sum = 0 ;      // varibale for 'running total'
  int ret ;

  printf ("Put in the first integer: ") ;
  ret = scanf("%d", &x) ;
  if (ret != 1) {
    printf ("You have not inserted an integer.\nThe average does not exist.\n");
  } else {
    sum = sum + x ;
    
    while (ret == 1 ) {
      printf ("Put in the next integer:");
      ret = scanf ("%d", &x);
      if ( ret == 1 ) {sum = sum + x; count++;} 
    }
    double average ;
    average = sum / count ;
    printf ("The average is %.2lf.\n", average) ;
  }
 return EXIT_SUCCESS;
}
