#include <stdio.h>           // Loana Gericke, s1643661, 24.10.16, Forresthill
#include <stdlib.h>

int main(void) {
  double n;
  printf ("Of how many integers do you want to calculate the average? ");
  scanf ("%lf", &n);

  double sum, x, count ;
  for ( sum = 0 ; count < n ; count++ ) {
    printf ("Enter an integer: ") ;
    scanf ("%lf", &x) ;
    sum = sum + x ;
  }

  double average;
  average = sum / n ;
 
  printf ("The average is %.2lf.\n", average);

 return EXIT_SUCCESS;
}
