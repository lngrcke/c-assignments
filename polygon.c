
#include <stdlib.h>           // Loana Gericke, s1643661, 31.10.16, ForrestHill
#include <stdio.h>
#include "descartes.h"

int main(void)
{
   point_t   curr,  /* Current point */ 
             prev,  /* Previous point */
             init;  /* Initial point */
   lineSeg_t l,     /* Line segment joining prev and curr */
             m;     /*Line segment joining the first and last vertex */  

   OpenGraphics();
   prev = GetPoint();
   curr = GetPoint();
   init = prev;   

   double cumulativeLength = 0.0;

   while (XCoord(curr) >= 0) {
     l = LineSeg(prev, curr);
     DrawLineSeg(l);

     double Length(lineSeg_t l);
     cumulativeLength = cumulativeLength + Length(l);

     prev = curr;
     curr = GetPoint();
   }
  
   m = LineSeg(init, prev);
   DrawLineSeg(m);

   double Length(lineSeg_t m);
   cumulativeLength = cumulativeLength + Length(m);
   CloseGraphics();

   printf ("The perimeter of the polygon is %lf.\n", cumulativeLength);

   
   return EXIT_SUCCESS;
}

