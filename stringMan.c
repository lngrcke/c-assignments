#include <stdio.h>     // Loana Gericke, s1643661, 07.11.2016, Forresthill
#include <stdlib.h>
#include <string.h>
#define MAXLEN 100

void function1(char* str) {
  int i = strlen(str);
  int temp[i];
  int k;

  for (k = 0; k < (i * 0.5); ++k) { temp[k] = str[k]; }
  for (k = 0; k < (i * 0.5); ++k) { str[k] = str[i - 1 - k];}
  for (k = i * 0.5; k < i; ++k) {   str[k] = temp[i - 1 -k];}
}

void function2(char* str, char drop, char sub){
  int i = strlen(str);
  int k;
  for (k = 0; k < i; ++k) {if (str[k] == drop){str[k]=sub;}}
}

int function3(char* str){
  int i = strlen(str);
  int k;
  for (k = 0; k < (i * 0.5); ++k){
    if (str[k] != str[i-(k+1)]){
      return 0;
    } 
  }
  return 1;  
}

int main(void){
  int i;
  printf ("Which function would you like to use?\n");
  printf ("To reverse a given string type 1.\n");
  printf ("To substitute one character with another type 2.\n");
  printf ("To decide whether or not the string is a palindrome type 3.\n");
  printf ("Please insert your choice: ");
  scanf ("%d", &i);

  if (i < 1 || i > 3) {printf ("This is no valid choice.\n");}
  else if (i == 1 ){
    char a[MAXLEN];
    printf ("Please insert your string: ");
    scanf ("%s", a); 
    function1(a);
    printf ("%s.\n", a);
 
  } else if (i == 2 ){
    char a[MAXLEN];
    char drop, sub;
    printf("Please insert your string: ");
    scanf ("%s", a);
    printf("Please insert a character which will be dropped:");
    scanf (" %c", &drop);
    printf("Please insert a character which will be substituted in:");
    scanf (" %c", &sub);

    function2(a, drop, sub);
    printf("%s.\n", a);

  } else {
    char a[MAXLEN];
    printf("Please insert your string: ");
    scanf ("%s", a);

    if (function3(a) == 1){
      printf ("palindrome\n");
    } else {
      printf ("not a palindrome\n");
    }
  }
 return EXIT_SUCCESS;
}
